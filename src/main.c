/* Created by Anjuta version 1.2.2 */
/*	This file will not be overwritten */

#include <gnome.h>
#include "config.h"
#include "eggtrayicon.h"
#include "sys/types.h"
#include "sys/wait.h"

EggTrayIcon *tray_icon = NULL;
GtkWidget *icon = NULL;
GtkWidget *menu = NULL;
guint oopid = 0;
char *ooargs[]={"ooffice", "-plugin", "-quickstart", NULL};
gboolean end_process = FALSE;

static void create_menu ()
{
	GtkWidget *menuitem;
	
	menu = gtk_menu_new ();

/*
	menuitem = gtk_menu_item_new_with_label ("1337 level");
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	menuitem = gtk_menu_item_new_with_label ("Novice level");
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	menuitem = gtk_menu_item_new_with_label ("Master level");
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);

	menuitem = gtk_separator_menu_item_new ();
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
*/
	menuitem = gtk_menu_item_new_with_label (_("Exit"));
	g_signal_connect (G_OBJECT (menuitem), "activate", G_CALLBACK (gtk_main_quit), 0);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	
	gtk_widget_show_all (menu);
}
 
static void tray_icon_load ()
{
	GdkPixbuf *pixbuf;

	pixbuf = gtk_image_get_pixbuf (GTK_IMAGE (icon));

	if (pixbuf)
		g_object_unref (G_OBJECT (pixbuf));
	
	pixbuf = gdk_pixbuf_new_from_file (PACKAGE_PIXMAPS_DIR "/icon.png", NULL);
			
	gtk_image_set_from_pixbuf (GTK_IMAGE (icon), pixbuf);
}

static void clicked (GtkWidget *widget, GdkEventButton *event, gpointer data)
{
	gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL, NULL, event->button, event->time);
}

void tray_create ()
{
	GtkWidget *box;
	
	if (!menu)
		create_menu ();
	
	tray_icon = egg_tray_icon_new ("Lukas' Private Stash");
	box = gtk_event_box_new ();
	icon = gtk_image_new ();
	 
	gtk_container_add (GTK_CONTAINER (box), icon);
	gtk_container_add (GTK_CONTAINER (tray_icon), box);
	gtk_widget_show_all (GTK_WIDGET (tray_icon));
	
	g_signal_connect (G_OBJECT (box), "button-press-event", G_CALLBACK (clicked), NULL);
	
	tray_icon_load ();
}

void tray_destroy ()
{
	GdkPixbuf *pixbuf;
	
	pixbuf = gtk_image_get_pixbuf (GTK_IMAGE (icon));

	if (pixbuf)
		g_object_unref (G_OBJECT (pixbuf));
	
	gtk_widget_destroy (icon);
	
	gtk_widget_destroy (GTK_WIDGET (tray_icon));
}

gboolean isRunning(){
	int result;
	
	if (oopid == 0) return FALSE;
	g_message("Checking PID %i", oopid);
	
	result = kill(oopid, 0);
	
	if (result == 0) g_message("OO is still running");
	else g_message("OO is NOT running");
	
	return result == 0;
}

gboolean launch_process(gpointer data) {
	if (end_process) return FALSE;

	if (!isRunning()) {
		g_message("exec oo");
		oopid = gnome_execute_async(NULL, 3, ooargs);
		g_message("oo running with PID %i", oopid);
	}
	
	return TRUE;
}		

void stop_process() {
	end_process = TRUE;
	if (isRunning() && oopid != 0) {
   		if (oopid != 0)  {
			g_message("finishing oo");
			kill(oopid, SIGTERM);
			g_message("oo terminated");
		} else {
			g_message("oo is not running");
		}
	}
}

void start_process() {
	g_timeout_add(5000, launch_process, NULL);
}

int main(int argc, char *argv[])
{

	gtk_init (&argc, &argv);

	tray_create ();
	
	start_process();
	
	gtk_main ();
	
	stop_process();

	tray_destroy ();

	return 0;
}
